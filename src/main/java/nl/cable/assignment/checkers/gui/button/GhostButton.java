package nl.cable.assignment.checkers.gui.button;

import nl.cable.assignment.checkers.ai.AI;
import nl.cable.assignment.checkers.board.BoardState;
import nl.cable.assignment.checkers.gui.GuiSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Button representing a possible move for a player
 */

public class GhostButton extends JButton{

    Logger logger
            = Logger.getLogger(
            GhostButton.class.getName());
    private BoardState boardstate;

    public GhostButton(BoardState state){
        super();
        this.boardstate = state;
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setContentAreaFilled(false);
        setIcon();
    }

    private void setIcon(){
        BufferedImage buttonIcon = null;
        try{
            if(GuiSettings.helpMode){
                buttonIcon = ImageIO.read(new File(getClass().getResource("/images/dottedcircle.png").getFile()));

            }
            else{
                buttonIcon = ImageIO.read(new File(getClass().getResource("/images/dottedcircleblack.png").getFile()));
            }
        }
        catch (IOException e){
            logger.info(e.toString());
        }
        if (buttonIcon != null){
            Image resized = buttonIcon.getScaledInstance(GuiSettings.ghostButtonWidth, GuiSettings.ghostButtonHeight,100);
            ImageIcon icon = new ImageIcon(resized);
            this.setIcon(icon);
        }
    }


    public BoardState getBoardstate() {
        return boardstate;
    }
}
