package nl.cable.assignment.checkers.gui.button;


import nl.cable.assignment.checkers.enums.Colour;
import nl.cable.assignment.checkers.enums.MoveFeedback;
import nl.cable.assignment.checkers.enums.Player;
import nl.cable.assignment.checkers.gui.GUI;
import nl.cable.assignment.checkers.gui.GuiSettings;
import nl.cable.assignment.checkers.pieces.Piece;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Black or white checker piece (clickable button component)
 */

public class CheckerButton extends JButton{

    private int position;
    private Piece piece;
    // drag drop
    int X;
    int Y;
    int screenX = 0;
    int screenY = 0;


    public CheckerButton(Piece piece, GUI gui){
        super();
        this.piece = piece;
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setContentAreaFilled(false);
        setIcon(piece);
        if (piece.getPlayer() == Player.HUMAN && GuiSettings.dragDrop){
            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent mouseEvent) {
                    screenX = mouseEvent.getXOnScreen();
                    screenY = mouseEvent.getYOnScreen();
                    X = getX();
                    Y = getY();
                }
                @Override
                public void mouseReleased(MouseEvent mouseEvent){
                    int deltaX = mouseEvent.getXOnScreen() - screenX;
                    int deltaY = mouseEvent.getYOnScreen() - screenY;
                    int dx = (int) Math.round((double)deltaX / (double) GuiSettings.squareSize);
                    int dy = (int) Math.round((double)deltaY / (double) GuiSettings.squareSize);
                    gui.onMouseRelease(position, dx, dy);
                }
            });
            this.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseDragged(MouseEvent mouseEvent) {
                    int deltaX = mouseEvent.getXOnScreen() - screenX;
                    int deltaY = mouseEvent.getYOnScreen() - screenY;
                    setLocation(X + deltaX, Y + deltaY);
                }
            });
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public Piece getPiece() {
        return piece;
    }

    private void setIcon(Piece piece){
        BufferedImage buttonIcon = null;
        Colour colour = GuiSettings.getColour(piece.getPlayer());
        try {
            if (colour == Colour.BLACK) {
                if (piece.isKing()) {
                    buttonIcon = ImageIO.read(new File(getClass().getResource("/images/blackking.png").getFile()));
                } else {
                    buttonIcon = ImageIO.read(new File(getClass().getResource("/images/blackchecker.gif").getFile()));
                }
            }
            else {
                if (piece.isKing()) {
                    buttonIcon = ImageIO.read(new File(getClass().getResource("/images/whiteking.png").getFile()));
                }
                else {
                    buttonIcon = ImageIO.read(new File(getClass().getResource("/images/whitechecker.gif").getFile()));
                }
            }
        }
        catch(IOException e){
            throw new RuntimeException(MoveFeedback.FORCED_JUMP.getName());
        }

        if (buttonIcon != null){
            Image resized = buttonIcon.getScaledInstance(GuiSettings.checkerWidth, GuiSettings.checkerHeight,100);
            ImageIcon icon = new ImageIcon(resized);
            this.setIcon(icon);
        }
    }

}
